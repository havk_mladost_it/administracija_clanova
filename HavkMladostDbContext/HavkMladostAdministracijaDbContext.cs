﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace HavkMladostDbContext
{
    public partial class HavkMladostAdministracijaDbContext : DbContext
    {
        public HavkMladostAdministracijaDbContext()
        {
        }

        public HavkMladostAdministracijaDbContext(DbContextOptions<HavkMladostAdministracijaDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AktivnostiSudjelovanje> AktivnostiSudjelovanjes { get; set; }
        public virtual DbSet<Clan> Clans { get; set; }
        public virtual DbSet<Clanarine> Clanarines { get; set; }
        public virtual DbSet<DodatniProizvoljniUnosi> DodatniProizvoljniUnosis { get; set; }
        public virtual DbSet<Duznosti> Duznostis { get; set; }
        public virtual DbSet<Karakteristike> Karakteristikes { get; set; }
        public virtual DbSet<KarakteristikeSportasa> KarakteristikeSportasas { get; set; }
        public virtual DbSet<LijecnickiPregledi> LijecnickiPregledis { get; set; }
        public virtual DbSet<Obrazovanje> Obrazovanjes { get; set; }
        public virtual DbSet<PodaciSkrbnika> PodaciSkrbnikas { get; set; }
        public virtual DbSet<PripadnostSekciji> PripadnostSekcijis { get; set; }
        public virtual DbSet<Sekcija> Sekcijas { get; set; }
        public virtual DbSet<TipoviAktivnosti> TipoviAktivnostis { get; set; }
        public virtual DbSet<TipoviClanarina> TipoviClanarinas { get; set; }
        public virtual DbSet<TipoviDuznosti> TipoviDuznostis { get; set; }
        public virtual DbSet<TipoviKategorizacije> TipoviKategorizacijes { get; set; }
        public virtual DbSet<TipoviZapisa> TipoviZapisas { get; set; }
        public virtual DbSet<Treneri> Treneris { get; set; }
        public virtual DbSet<TreneriDodjele> TreneriDodjeles { get; set; }
        public virtual DbSet<UplataClanarine> UplataClanarines { get; set; }
        public virtual DbSet<ZapisiPoClanu> ZapisiPoClanus { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=tcp:havk-mladost.database.windows.net,1433;Initial Catalog=havk_mladost_administracija_dev;Persist Security Info=False;User ID=havk_mladost;Password=administracijaKorisnika2019;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<AktivnostiSudjelovanje>(entity =>
            {
                entity.ToTable("AktivnostiSudjelovanje");

                entity.HasIndex(e => e.ClanId, "IX_AktivnostiSudjelovanje_ClanId");

                entity.HasIndex(e => e.TipoviAktivnostiId, "IX_AktivnostiSudjelovanje_TipoviAKtivnostiId");

                entity.Property(e => e.DatumAktivnosti).HasColumnType("date");

                entity.Property(e => e.TipoviAktivnostiId).HasColumnName("TipoviAKtivnostiId");

                entity.HasOne(d => d.Clan)
                    .WithMany(p => p.AktivnostiSudjelovanjes)
                    .HasForeignKey(d => d.ClanId);

                entity.HasOne(d => d.TipoviAktivnosti)
                    .WithMany(p => p.AktivnostiSudjelovanjes)
                    .HasForeignKey(d => d.TipoviAktivnostiId);
            });

            modelBuilder.Entity<Clan>(entity =>
            {
                entity.ToTable("Clan");

                entity.HasIndex(e => e.SkrbnikId, "IX_Clan_SkrbnikId")
                    .IsUnique()
                    .HasFilter("([SkrbnikId] IS NOT NULL)");

                entity.Property(e => e.Adresa)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.AkademskaTitula).HasMaxLength(30);

                entity.Property(e => e.Email)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Ime)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.Oib)
                    .IsRequired()
                    .HasMaxLength(11)
                    .IsUnicode(false)
                    .HasColumnName("OIB")
                    .IsFixedLength(true);

                entity.Property(e => e.Prezime)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.RegistracijaHvs)
                    .HasMaxLength(30)
                    .HasColumnName("RegistracijaHVS");

                entity.Property(e => e.Rodendan).HasColumnType("date");

                entity.Property(e => e.Spol)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsFixedLength(true);

                entity.Property(e => e.Telefon)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Zanimanje).HasMaxLength(30);

                entity.Property(e => e.Zvanje).HasMaxLength(30);

                entity.HasOne(d => d.Skrbnik)
                    .WithOne(p => p.Clan)
                    .HasForeignKey<Clan>(d => d.SkrbnikId);
            });

            modelBuilder.Entity<Clanarine>(entity =>
            {
                entity.ToTable("Clanarine");

                entity.HasIndex(e => e.NositeljGrupneId, "IX_Clanarine_NositeljGrupneId");

                entity.HasIndex(e => e.TipoviClanarinaId, "IX_Clanarine_TipoviClanarinaId");

                entity.Property(e => e.Do).HasColumnType("date");

                entity.Property(e => e.Od).HasColumnType("date");

                entity.HasOne(d => d.NositeljGrupneNavigation)
                    .WithMany(p => p.Clanarines)
                    .HasForeignKey(d => d.NositeljGrupneId);

                entity.HasOne(d => d.TipoviClanarina)
                    .WithMany(p => p.Clanarines)
                    .HasForeignKey(d => d.TipoviClanarinaId);
            });

            modelBuilder.Entity<DodatniProizvoljniUnosi>(entity =>
            {
                entity.ToTable("DodatniProizvoljniUnosi");

                entity.HasIndex(e => e.ClanId, "IX_DodatniProizvoljniUnosi_ClanId");

                entity.Property(e => e.DatumZapisa).HasColumnType("date");

                entity.HasOne(d => d.Clan)
                    .WithMany(p => p.DodatniProizvoljniUnosis)
                    .HasForeignKey(d => d.ClanId);
            });

            modelBuilder.Entity<Duznosti>(entity =>
            {
                entity.ToTable("Duznosti");

                entity.HasIndex(e => e.ClanId, "IX_Duznosti_ClanId");

                entity.HasIndex(e => e.TipoviDuznostiId, "IX_Duznosti_TipoviDuznostiId");

                entity.Property(e => e.Do).HasColumnType("date");

                entity.Property(e => e.Od).HasColumnType("date");

                entity.HasOne(d => d.Clan)
                    .WithMany(p => p.Duznostis)
                    .HasForeignKey(d => d.ClanId);

                entity.HasOne(d => d.TipoviDuznosti)
                    .WithMany(p => p.Duznostis)
                    .HasForeignKey(d => d.TipoviDuznostiId);
            });

            modelBuilder.Entity<Karakteristike>(entity =>
            {
                entity.ToTable("Karakteristike");

                entity.Property(e => e.MjernaJedinica).HasMaxLength(10);

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.Opis).HasMaxLength(200);
            });

            modelBuilder.Entity<KarakteristikeSportasa>(entity =>
            {
                entity.ToTable("KarakteristikeSportasa");

                entity.HasIndex(e => e.ClanId, "IX_KarakteristikeSportasa_ClanId");

                entity.HasIndex(e => e.KarakteristikeId, "IX_KarakteristikeSportasa_KarakteristikeId");

                entity.Property(e => e.DatumOpisa).HasColumnType("date");

                entity.Property(e => e.Vrijednost).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.Clan)
                    .WithMany(p => p.KarakteristikeSportasas)
                    .HasForeignKey(d => d.ClanId);

                entity.HasOne(d => d.Karakteristike)
                    .WithMany(p => p.KarakteristikeSportasas)
                    .HasForeignKey(d => d.KarakteristikeId);
            });

            modelBuilder.Entity<LijecnickiPregledi>(entity =>
            {
                entity.ToTable("LijecnickiPregledi");

                entity.HasIndex(e => e.ClanId, "IX_LijecnickiPregledi_ClanId");

                entity.Property(e => e.VrijediDo).HasColumnType("date");

                entity.Property(e => e.VrijediOd).HasColumnType("date");

                entity.HasOne(d => d.Clan)
                    .WithMany(p => p.LijecnickiPregledis)
                    .HasForeignKey(d => d.ClanId);
            });

            modelBuilder.Entity<Obrazovanje>(entity =>
            {
                entity.ToTable("Obrazovanje");

                entity.HasIndex(e => e.ClanId, "IX_Obrazovanje_ClanId");

                entity.Property(e => e.Do).HasColumnType("date");

                entity.Property(e => e.ObrazovnaUstanova)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.Od).HasColumnType("date");

                entity.HasOne(d => d.Clan)
                    .WithMany(p => p.Obrazovanjes)
                    .HasForeignKey(d => d.ClanId);
            });

            modelBuilder.Entity<PodaciSkrbnika>(entity =>
            {
                entity.HasKey(e => e.SkrbnikId);

                entity.ToTable("PodaciSkrbnika");

                entity.Property(e => e.Adresa)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Email)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Ime)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.Oib)
                    .IsRequired()
                    .HasMaxLength(11)
                    .IsUnicode(false)
                    .HasColumnName("OIB")
                    .IsFixedLength(true);

                entity.Property(e => e.Prezime)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.Spol)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsFixedLength(true);

                entity.Property(e => e.Telefon)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PripadnostSekciji>(entity =>
            {
                entity.ToTable("PripadnostSekciji");

                entity.HasIndex(e => e.ClanId, "IX_PripadnostSekciji_ClanId");

                entity.HasIndex(e => e.SekcijaId, "IX_PripadnostSekciji_SekcijaId");

                entity.HasIndex(e => e.TipoviKategorizacijeId, "IX_PripadnostSekciji_TipoviKategorizacijeId");

                entity.HasIndex(e => e.TreneriId, "IX_PripadnostSekciji_TreneriId");

                entity.Property(e => e.Do).HasColumnType("date");

                entity.Property(e => e.Od).HasColumnType("date");

                entity.HasOne(d => d.Clan)
                    .WithMany(p => p.PripadnostSekcijis)
                    .HasForeignKey(d => d.ClanId);

                entity.HasOne(d => d.Sekcija)
                    .WithMany(p => p.PripadnostSekcijis)
                    .HasForeignKey(d => d.SekcijaId);

                entity.HasOne(d => d.TipoviKategorizacije)
                    .WithMany(p => p.PripadnostSekcijis)
                    .HasForeignKey(d => d.TipoviKategorizacijeId);

                entity.HasOne(d => d.Treneri)
                    .WithMany(p => p.PripadnostSekcijis)
                    .HasForeignKey(d => d.TreneriId);
            });

            modelBuilder.Entity<Sekcija>(entity =>
            {
                entity.ToTable("Sekcija");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.Opis).HasMaxLength(100);

                entity.Property(e => e.Sifra)
                    .IsRequired()
                    .HasMaxLength(5);
            });

            modelBuilder.Entity<TipoviAktivnosti>(entity =>
            {
                entity.ToTable("TipoviAktivnosti");

                entity.Property(e => e.NazivAktivnosti)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.Opis)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<TipoviClanarina>(entity =>
            {
                entity.ToTable("TipoviClanarina");

                entity.Property(e => e.IznosClanarine).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.NazivClanarine)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.Opis).HasMaxLength(100);
            });

            modelBuilder.Entity<TipoviDuznosti>(entity =>
            {
                entity.ToTable("TipoviDuznosti");

                entity.Property(e => e.NazivDuznosti)
                    .IsRequired()
                    .HasMaxLength(40);

                entity.Property(e => e.Opis)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<TipoviKategorizacije>(entity =>
            {
                entity.ToTable("TipoviKategorizacije");

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<TipoviZapisa>(entity =>
            {
                entity.ToTable("TipoviZapisa");

                entity.Property(e => e.KodZapisa)
                    .IsRequired()
                    .HasMaxLength(5);

                entity.Property(e => e.Naziv)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.Opis).HasMaxLength(100);
            });

            modelBuilder.Entity<Treneri>(entity =>
            {
                entity.ToTable("Treneri");

                entity.HasIndex(e => e.ClanId, "IX_Treneri_ClanId")
                    .IsUnique()
                    .HasFilter("([ClanId] IS NOT NULL)");

                entity.Property(e => e.Ime)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.Prezime)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.HasOne(d => d.Clan)
                    .WithOne(p => p.Treneri)
                    .HasForeignKey<Treneri>(d => d.ClanId);
            });

            modelBuilder.Entity<TreneriDodjele>(entity =>
            {
                entity.ToTable("TreneriDodjele");

                entity.HasIndex(e => e.SekcijaId, "IX_TreneriDodjele_SekcijaId");

                entity.HasIndex(e => e.TreneriId, "IX_TreneriDodjele_TreneriId");

                entity.Property(e => e.Do).HasColumnType("date");

                entity.Property(e => e.GodineStaza).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.KategorijaTrenera)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.Kvalifikacije)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Od).HasColumnType("date");

                entity.Property(e => e.ReprezentacijaPosada).HasMaxLength(50);

                entity.HasOne(d => d.Sekcija)
                    .WithMany(p => p.TreneriDodjeles)
                    .HasForeignKey(d => d.SekcijaId);

                entity.HasOne(d => d.Treneri)
                    .WithMany(p => p.TreneriDodjeles)
                    .HasForeignKey(d => d.TreneriId);
            });

            modelBuilder.Entity<UplataClanarine>(entity =>
            {
                entity.ToTable("UplataClanarine");

                entity.HasIndex(e => e.ClanId, "IX_UplataClanarine_ClanId");

                entity.Property(e => e.DatumUplate).HasColumnType("date");

                entity.Property(e => e.MjesecGodina).HasColumnType("date");

                entity.HasOne(d => d.Clan)
                    .WithMany(p => p.UplataClanarines)
                    .HasForeignKey(d => d.ClanId);
            });

            modelBuilder.Entity<ZapisiPoClanu>(entity =>
            {
                entity.ToTable("ZapisiPoClanu");

                entity.HasIndex(e => e.ClanId, "IX_ZapisiPoClanu_ClanId");

                entity.HasIndex(e => e.TipoviZapisaId, "IX_ZapisiPoClanu_TipoviZapisaId");

                entity.Property(e => e.DatumZapisa).HasColumnType("date");

                entity.Property(e => e.Opis).HasMaxLength(100);

                entity.HasOne(d => d.Clan)
                    .WithMany(p => p.ZapisiPoClanus)
                    .HasForeignKey(d => d.ClanId);

                entity.HasOne(d => d.TipoviZapisa)
                    .WithMany(p => p.ZapisiPoClanus)
                    .HasForeignKey(d => d.TipoviZapisaId);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
