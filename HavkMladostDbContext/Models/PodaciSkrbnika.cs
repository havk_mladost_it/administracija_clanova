﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HavkMladostDbContext
{
    public partial class PodaciSkrbnika
    {
        public int SkrbnikId { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Spol { get; set; }
        public string Oib { get; set; }
        public string Telefon { get; set; }
        public string Email { get; set; }
        public string Adresa { get; set; }

        public virtual Clan Clan { get; set; }
    }
}
