﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HavkMladostDbContext
{
    public partial class TipoviZapisa
    {
        public TipoviZapisa()
        {
            ZapisiPoClanus = new HashSet<ZapisiPoClanu>();
        }

        public int TipoviZapisaId { get; set; }
        public string KodZapisa { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }
        public bool Aktivan { get; set; }

        public virtual ICollection<ZapisiPoClanu> ZapisiPoClanus { get; set; }
    }
}
