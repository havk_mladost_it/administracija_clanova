﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HavkMladostDbContext
{
    public partial class PripadnostSekciji
    {
        public int PripadnostSekcijiId { get; set; }
        public bool RedovniClan { get; set; }
        public DateTime Od { get; set; }
        public DateTime Do { get; set; }
        public int ClanId { get; set; }
        public int? TipoviKategorizacijeId { get; set; }
        public int SekcijaId { get; set; }
        public int TreneriId { get; set; }

        public virtual Clan Clan { get; set; }
        public virtual Sekcija Sekcija { get; set; }
        public virtual TipoviKategorizacije TipoviKategorizacije { get; set; }
        public virtual Treneri Treneri { get; set; }
    }
}
