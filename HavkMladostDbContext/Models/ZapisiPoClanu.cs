﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HavkMladostDbContext
{
    public partial class ZapisiPoClanu
    {
        public int ZapisiPoClanuId { get; set; }
        public DateTime DatumZapisa { get; set; }
        public string Opis { get; set; }
        public int ClanId { get; set; }
        public int TipoviZapisaId { get; set; }

        public virtual Clan Clan { get; set; }
        public virtual TipoviZapisa TipoviZapisa { get; set; }
    }
}
