﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HavkMladostDbContext
{
    public partial class TipoviKategorizacije
    {
        public TipoviKategorizacije()
        {
            PripadnostSekcijis = new HashSet<PripadnostSekciji>();
        }

        public int TipoviKategorizacijeId { get; set; }
        public string Naziv { get; set; }
        public bool Aktivno { get; set; }

        public virtual ICollection<PripadnostSekciji> PripadnostSekcijis { get; set; }
    }
}
