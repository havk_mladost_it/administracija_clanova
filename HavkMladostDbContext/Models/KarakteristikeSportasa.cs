﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HavkMladostDbContext
{
    public partial class KarakteristikeSportasa
    {
        public int KarakteristikeSportasaId { get; set; }
        public decimal Vrijednost { get; set; }
        public DateTime DatumOpisa { get; set; }
        public int ClanId { get; set; }
        public int KarkateristikeId { get; set; }
        public int? KarakteristikeId { get; set; }

        public virtual Clan Clan { get; set; }
        public virtual Karakteristike Karakteristike { get; set; }
    }
}
