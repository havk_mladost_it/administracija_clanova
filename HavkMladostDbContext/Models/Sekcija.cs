﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HavkMladostDbContext
{
    public partial class Sekcija
    {
        public Sekcija()
        {
            PripadnostSekcijis = new HashSet<PripadnostSekciji>();
            TreneriDodjeles = new HashSet<TreneriDodjele>();
        }

        public int SekcijaId { get; set; }
        public string Sifra { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }
        public bool Aktivno { get; set; }
        public bool? Natjecateljska { get; set; }

        public virtual ICollection<PripadnostSekciji> PripadnostSekcijis { get; set; }
        public virtual ICollection<TreneriDodjele> TreneriDodjeles { get; set; }
    }
}
