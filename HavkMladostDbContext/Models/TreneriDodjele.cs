﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HavkMladostDbContext
{
    public partial class TreneriDodjele
    {
        public int TreneriDodjeleId { get; set; }
        public string Kvalifikacije { get; set; }
        public decimal GodineStaza { get; set; }
        public bool? Reprezentacija { get; set; }
        public string KategorijaTrenera { get; set; }
        public string ReprezentacijaPosada { get; set; }
        public DateTime Od { get; set; }
        public DateTime Do { get; set; }
        public int SekcijaId { get; set; }
        public int TreneriId { get; set; }

        public virtual Sekcija Sekcija { get; set; }
        public virtual Treneri Treneri { get; set; }
    }
}
