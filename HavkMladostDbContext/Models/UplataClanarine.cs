﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HavkMladostDbContext
{
    public partial class UplataClanarine
    {
        public int UplataClanarineId { get; set; }
        public DateTime DatumUplate { get; set; }
        public DateTime MjesecGodina { get; set; }
        public int ClanId { get; set; }

        public virtual Clan Clan { get; set; }
    }
}
