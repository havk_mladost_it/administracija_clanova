﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HavkMladostDbContext
{
    public partial class Clan
    {
        public Clan()
        {
            AktivnostiSudjelovanjes = new HashSet<AktivnostiSudjelovanje>();
            Clanarines = new HashSet<Clanarine>();
            DodatniProizvoljniUnosis = new HashSet<DodatniProizvoljniUnosi>();
            Duznostis = new HashSet<Duznosti>();
            KarakteristikeSportasas = new HashSet<KarakteristikeSportasa>();
            LijecnickiPregledis = new HashSet<LijecnickiPregledi>();
            Obrazovanjes = new HashSet<Obrazovanje>();
            PripadnostSekcijis = new HashSet<PripadnostSekciji>();
            UplataClanarines = new HashSet<UplataClanarine>();
            ZapisiPoClanus = new HashSet<ZapisiPoClanu>();
        }

        public int ClanId { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Spol { get; set; }
        public string Oib { get; set; }
        public DateTime Rodendan { get; set; }
        public string Telefon { get; set; }
        public string Email { get; set; }
        public string Adresa { get; set; }
        public string Zvanje { get; set; }
        public string Zanimanje { get; set; }
        public string AkademskaTitula { get; set; }
        public string RegistracijaHvs { get; set; }
        public int? SkrbnikId { get; set; }

        public virtual PodaciSkrbnika Skrbnik { get; set; }
        public virtual Treneri Treneri { get; set; }
        public virtual ICollection<AktivnostiSudjelovanje> AktivnostiSudjelovanjes { get; set; }
        public virtual ICollection<Clanarine> Clanarines { get; set; }
        public virtual ICollection<DodatniProizvoljniUnosi> DodatniProizvoljniUnosis { get; set; }
        public virtual ICollection<Duznosti> Duznostis { get; set; }
        public virtual ICollection<KarakteristikeSportasa> KarakteristikeSportasas { get; set; }
        public virtual ICollection<LijecnickiPregledi> LijecnickiPregledis { get; set; }
        public virtual ICollection<Obrazovanje> Obrazovanjes { get; set; }
        public virtual ICollection<PripadnostSekciji> PripadnostSekcijis { get; set; }
        public virtual ICollection<UplataClanarine> UplataClanarines { get; set; }
        public virtual ICollection<ZapisiPoClanu> ZapisiPoClanus { get; set; }
    }
}
