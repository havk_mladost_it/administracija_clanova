﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HavkMladostDbContext
{
    public partial class Duznosti
    {
        public int DuznostiId { get; set; }
        public DateTime Od { get; set; }
        public DateTime Do { get; set; }
        public int ClanId { get; set; }
        public int TipoviDuznostiId { get; set; }

        public virtual Clan Clan { get; set; }
        public virtual TipoviDuznosti TipoviDuznosti { get; set; }
    }
}
