﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HavkMladostDbContext
{
    public partial class TipoviDuznosti
    {
        public TipoviDuznosti()
        {
            Duznostis = new HashSet<Duznosti>();
        }

        public int TipoviDuznostiId { get; set; }
        public string NazivDuznosti { get; set; }
        public string Opis { get; set; }
        public bool Aktivan { get; set; }

        public virtual ICollection<Duznosti> Duznostis { get; set; }
    }
}
