﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HavkMladostDbContext
{
    public partial class Treneri
    {
        public Treneri()
        {
            PripadnostSekcijis = new HashSet<PripadnostSekciji>();
            TreneriDodjeles = new HashSet<TreneriDodjele>();
        }

        public int TreneriId { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public bool Aktivan { get; set; }
        public int? ClanId { get; set; }

        public virtual Clan Clan { get; set; }
        public virtual ICollection<PripadnostSekciji> PripadnostSekcijis { get; set; }
        public virtual ICollection<TreneriDodjele> TreneriDodjeles { get; set; }
    }
}
