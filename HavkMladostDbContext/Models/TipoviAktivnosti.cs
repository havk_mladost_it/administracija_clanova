﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HavkMladostDbContext
{
    public partial class TipoviAktivnosti
    {
        public TipoviAktivnosti()
        {
            AktivnostiSudjelovanjes = new HashSet<AktivnostiSudjelovanje>();
        }

        public int TipoviAktivnostiId { get; set; }
        public string NazivAktivnosti { get; set; }
        public string Opis { get; set; }
        public bool Aktivan { get; set; }

        public virtual ICollection<AktivnostiSudjelovanje> AktivnostiSudjelovanjes { get; set; }
    }
}
