﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HavkMladostDbContext
{
    public partial class AktivnostiSudjelovanje
    {
        public int AktivnostiSudjelovanjeId { get; set; }
        public DateTime DatumAktivnosti { get; set; }
        public int ClanId { get; set; }
        public int TipoviAktivnostiId { get; set; }

        public virtual Clan Clan { get; set; }
        public virtual TipoviAktivnosti TipoviAktivnosti { get; set; }
    }
}
