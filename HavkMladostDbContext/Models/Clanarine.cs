﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HavkMladostDbContext
{
    public partial class Clanarine
    {
        public int ClanarineId { get; set; }
        public DateTime Od { get; set; }
        public DateTime Do { get; set; }
        public bool NositeljGrupne { get; set; }
        public int NositeljGrupneId { get; set; }
        public int ClanId { get; set; }
        public int TipoviClanarinaId { get; set; }

        public virtual Clan NositeljGrupneNavigation { get; set; }
        public virtual TipoviClanarina TipoviClanarina { get; set; }
    }
}
