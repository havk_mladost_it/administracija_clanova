﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HavkMladostDbContext
{
    public partial class Obrazovanje
    {
        public int ObrazovanjeId { get; set; }
        public DateTime Od { get; set; }
        public DateTime Do { get; set; }
        public string ObrazovnaUstanova { get; set; }
        public int ClanId { get; set; }

        public virtual Clan Clan { get; set; }
    }
}
