﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HavkMladostDbContext
{
    public partial class Karakteristike
    {
        public Karakteristike()
        {
            KarakteristikeSportasas = new HashSet<KarakteristikeSportasa>();
        }

        public int KarakteristikeId { get; set; }
        public string Naziv { get; set; }
        public string Opis { get; set; }
        public string MjernaJedinica { get; set; }

        public virtual ICollection<KarakteristikeSportasa> KarakteristikeSportasas { get; set; }
    }
}
