﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HavkMladostDbContext
{
    public partial class TipoviClanarina
    {
        public TipoviClanarina()
        {
            Clanarines = new HashSet<Clanarine>();
        }

        public int TipoviClanarinaId { get; set; }
        public string NazivClanarine { get; set; }
        public string Opis { get; set; }
        public decimal IznosClanarine { get; set; }
        public bool Grupna { get; set; }
        public bool Aktivno { get; set; }

        public virtual ICollection<Clanarine> Clanarines { get; set; }
    }
}
