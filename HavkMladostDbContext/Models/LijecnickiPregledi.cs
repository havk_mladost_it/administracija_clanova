﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HavkMladostDbContext
{
    public partial class LijecnickiPregledi
    {
        public int LijecnickiPreglediId { get; set; }
        public DateTime VrijediOd { get; set; }
        public DateTime VrijediDo { get; set; }
        public int ClanId { get; set; }

        public virtual Clan Clan { get; set; }
    }
}
