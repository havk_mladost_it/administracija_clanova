﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HavkMladostDbContext
{
    public partial class DodatniProizvoljniUnosi
    {
        public int DodatniProizvoljniUnosiId { get; set; }
        public DateTime DatumZapisa { get; set; }
        public string Opis { get; set; }
        public int ClanId { get; set; }

        public virtual Clan Clan { get; set; }
    }
}
