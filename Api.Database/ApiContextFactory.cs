using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;


namespace Api.Database
{
public class ApiContextFactory : IDesignTimeDbContextFactory<ApiContext>
    {
        public ApiContextFactory()
        {
        }


        public ApiContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ApiContext>();
            builder.UseSqlServer(
                "Server=tcp:havk-mladost.database.windows.net,1433;Initial Catalog=havk_mladost_administracija_dev;Persist Security Info=False;User ID=havk_mladost;Password=administracijaKorisnika2019;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");

            return new ApiContext(builder.Options);
        }
    }
}