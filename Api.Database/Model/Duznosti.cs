using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Database.Model
{
    [Table("Duznosti")]
    public class Duznosti
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int DuznostiId { get; set; }

        [Column(TypeName = "date")]
        [Required]
        public DateTime Od { get; set; }

        [Column(TypeName = "date")]
        public DateTime Do { get; set; }

        [Required]
        public int ClanId { get; set; }
        public Clan Clan { get; set; }

        [Required]
        public int TipoviDuznostiId { get; set; }
        public TipoviDuznosti TipoviDuznosti { get; set; }

    }
}