using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Database.Model
{
    [Table("PodaciSkrbnika")]
    public class Skrbnik
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int SkrbnikId { get; set; }

        [Column(TypeName = "nvarchar(30)")]
        [Required]
        public string Ime { get; set; }

        [Column(TypeName = "nvarchar(30)")]
        [Required]
        public string Prezime { get; set; }

        [Column(TypeName = "nchar(1)")]
        [Required]
        public string Spol { get; set; }

        [Column(TypeName = "char(11)")]
        [Required]
        public string OIB { get; set; }

        [Column(TypeName = "varchar(20)")]
        public string Telefon { get; set; }

        [Column(TypeName = "varchar(30)")]
        public string Email { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        [Required]
        public string Adresa { get; set; }

        [Required]
        public Clan Clan { get; set; }
    }
}