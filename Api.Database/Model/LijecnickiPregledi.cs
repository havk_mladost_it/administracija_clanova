using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Database.Model
{
    [Table("LijecnickiPregledi")]
    public class LijecnickiPregledi
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int LijecnickiPreglediId { get; set; }

        [Column(TypeName = "date")]
        [Required]
        public DateTime VrijediOd { get; set; }

        [Column(TypeName = "date")]
        [Required]
        public DateTime VrijediDo { get; set; }

        public int ClanId { get; set; }
        public Clan Clan { get; set; }

    }
}