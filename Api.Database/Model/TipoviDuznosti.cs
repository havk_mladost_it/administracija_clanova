using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Database.Model
{
    [Table("TipoviDuznosti")]
    public class TipoviDuznosti
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int TipoviDuznostiId { get; set; }

        [Column(TypeName = "nvarchar(40)")]
        [Required]
        public string NazivDuznosti { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        [Required]
        public string Opis { get; set; }

        [Column(TypeName = "bit")]
        [Required]
        public bool Aktivan { get; set; }

        public ICollection<Duznosti> Duznosti { get; set; }
    }
}