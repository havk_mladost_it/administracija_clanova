using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace Api.Database.Model
{
    [Table("Sekcija")]
    public class Sekcija
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int SekcijaId { get; set; }

        [Column(TypeName = "nvarchar(5)")]
        [Required]
        public string Sifra { get; set; }

        [Column(TypeName = "nvarchar(30)")]
        [Required]
        public string Naziv { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public string Opis { get; set; }

        [Column(TypeName = "bit")]
        [Required]
        public bool Aktivno { get; set; }

        [Column(TypeName = "bit")]
        public bool? Natjecateljska { get; set; }
       public ICollection<PripadnostSekciji> PripadnostSekcijama { get; set; }
       public ICollection<TreneriDodjele> TreneriDodjele { get; set; }
    }
}