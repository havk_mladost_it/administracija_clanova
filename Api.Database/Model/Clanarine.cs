using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Database.Model
{
    [Table("Clanarine")]
    public class Clanarine
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ClanarineId { get; set; }

        [Column(TypeName = "date")]
        [Required]
        public DateTime Od { get; set; }

        [Column(TypeName = "date")]
        public DateTime Do { get; set; }

        [Column(TypeName = "bit")]
        public bool NositeljGrupne { get; set; }

        [ForeignKey("Clan")]
        public int NositeljGrupneId { get; set; }

        [Required]
        public int ClanId { get; set; }
        public Clan Clan { get; set; }

        [Required]
        public int TipoviClanarinaId { get; set; }
        public TipoviClanarina TipoviClanarina { get; set; }

    }
}