using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Database.Model
{
    [Table("ZapisiPoClanu")]
    public class ZapisiPoClanu
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ZapisiPoClanuId { get; set; }

        [Column(TypeName = "date")]
        [Required]
        public DateTime DatumZapisa { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public string Opis { get; set; }

        [Required]
        public int ClanId { get; set; }
        public Clan Clan { get; set; }

        [Required]
        public int TipoviZapisaId { get; set; }
        public TipoviZapisa TipoviZapisa { get; set; }

    }
}