using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Database.Model
{
    [Table("TipoviClanarina")]
    public class TipoviClanarina
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int TipoviClanarinaId { get; set; }

        [Column(TypeName = "nvarchar(30)")]
        [Required]
        public string NazivClanarine { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public string Opis { get; set; }

        [Required]
        public decimal IznosClanarine { get; set; }

        [Column(TypeName = "bit")]
        public bool Grupna { get; set; }

        [Column(TypeName = "bit")]
        public bool Aktivno { get; set; }

        public ICollection<Clanarine> Clanarine { get; set; }
    }
}