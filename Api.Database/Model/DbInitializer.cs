﻿using System;
using System.Linq;

namespace Api.Database.Model
{
    class DbInitializer
    {
        public static void Initialize(ApiContext context)
        {
            context.Database.EnsureCreated();

            // Look for any students.
            if (context.Clan.Any())
            {
                return;   // DB has been seeded
            }

            var clanovi = new Clan[]
            {
                new Clan{ Ime="Mario", Prezime="Rogulj", Spol = "Muško", OIB = "12345678901", Rodendan=DateTime.Parse("1987-03-09"),
                    Telefon = "0957540598", Email="mario.rogulj@gmail.com", Adresa ="Zagreb", Zvanje = "Magistar matematike", Zanimanje = "Programer",
                    AkademskaTitula = "mag.math", RegistracijaHVS ="" },
                new Clan{ Ime="Hrvoje", Prezime="Sabljić", Spol = "Muško", OIB = "23456789012", Rodendan=DateTime.Parse("1987-12-07"),
                    Telefon = "0995032669", Email="hsabljic@yahoo.com", Adresa ="Zagreb", Zvanje = "Magistar računarstva", Zanimanje = "Programer",
                    AkademskaTitula = "mag.prog", RegistracijaHVS ="" }
            };

            foreach (Clan s in clanovi) 
            {
                context.Clan.Add(s);
            }
            context.SaveChanges();

            var tipoviAktivnosti = new TipoviAktivnosti[]
            {
                new TipoviAktivnosti{NazivAktivnosti="Čišćenje okoliša kluba", Opis="", Aktivan = true},
                new TipoviAktivnosti{NazivAktivnosti="Popravljanje ergometara", Opis="", Aktivan = true},
                new TipoviAktivnosti{NazivAktivnosti="Sređivanje čamaca", Opis="", Aktivan = true},
            };

            foreach(TipoviAktivnosti s in tipoviAktivnosti)
            {
                context.TipoviAktivnosti.Add(s);
            }
            context.SaveChanges();

            var tipoviClanarina = new TipoviClanarina[]
            {
                new TipoviClanarina{NazivClanarine = "Puna", Opis = "Puna propisana članarina", IznosClanarine= 150m, Aktivno = true },
                new TipoviClanarina{NazivClanarine = "Studentska", Opis = "Članarina za studente", IznosClanarine= 80m, Aktivno = true },
                new TipoviClanarina{NazivClanarine = "Natjecateljska", Opis = "Članarina za veslače natjecatelje", IznosClanarine= 80m, Aktivno = true }
            };

            foreach(TipoviClanarina s in tipoviClanarina)
            {
                context.TipoviClanarina.Add(s);
            }
            context.SaveChanges();

            var tipoviDuznosti = new TipoviDuznosti[]
            {
                new TipoviDuznosti{ NazivDuznosti = "Predsjednik kluba", Opis = "", Aktivan = true},
                new TipoviDuznosti{ NazivDuznosti = "Član upravnog odbora", Opis = "", Aktivan = true},
                new TipoviDuznosti{ NazivDuznosti = "Predsjednik stručnog odbora", Opis = "", Aktivan = true}
            };

            foreach(TipoviDuznosti s in tipoviDuznosti)
            {
                context.TipoviDuznosti.Add(s);
            }
            context.SaveChanges();

            var tipoviZapisa = new TipoviZapisa[]
            {
                new TipoviZapisa{ KodZapisa = "DISC", Naziv = "Suspenzija", Opis ="Disciplinska mjera suspenzije", Aktivan = true},
                new TipoviZapisa{ KodZapisa = "DISC", Naziv = "Izbacivanje", Opis ="Disciplinska mjera izbacivanja iz kluba", Aktivan = true},
                new TipoviZapisa{ KodZapisa = "ZASL", Naziv = "Financijska donacija", Opis ="Financijska donacija klubu", Aktivan = true},
                new TipoviZapisa{ KodZapisa = "ZASL", Naziv = "Donacija", Opis ="Donacija dobara klubu", Aktivan = true}
            };

            foreach(TipoviZapisa s in tipoviZapisa)
            {
                context.TipoviZapisa.Add(s);
            }
            context.SaveChanges();

            var aktivnostiSudjelovanje = new AktivnostiSudjelovanje[]
            {
                new AktivnostiSudjelovanje{
                    ClanId = clanovi.Single(i => i.Prezime == "Sabljić").ClanId,
                    DatumAktivnosti = DateTime.Today,
                    TipoviAKtivnostiId = tipoviAktivnosti.Single(i => i.NazivAktivnosti == "Sređivanje čamaca").TipoviAktivnostiId}
            };

            foreach(AktivnostiSudjelovanje s in aktivnostiSudjelovanje)
            {
                context.AktivnostiSudjelovanje.Add(s);
            }
            context.SaveChanges();

            var clanarine = new Clanarine[]
            {
                new Clanarine{
                    ClanId = clanovi.Single(i => i.Prezime == "Sabljić").ClanId,
                    Od = DateTime.Parse("2019-01-01"),
                    TipoviClanarinaId = tipoviClanarina.Single(i => i.NazivClanarine == "Puna").TipoviClanarinaId
                },
                new Clanarine
                {
                    ClanId = clanovi.Single(i => i.Prezime == "Rogulj").ClanId,
                    Od = DateTime.Parse("2019-01-01"),
                    TipoviClanarinaId = tipoviClanarina.Single(i => i.NazivClanarine == "Puna").TipoviClanarinaId
                }
            };

            foreach(Clanarine s in clanarine)
            {
                context.Clanarine.Add(s);
            }
            context.SaveChanges();

            var dodatniProizvoljniUnosi = new DodatniProizvoljniUnosi[]
            {
                new DodatniProizvoljniUnosi{
                    ClanId = clanovi.Single(i=> i.Prezime == "Sabljić").ClanId,
                    DatumZapisa = DateTime.Parse("2019-02-01"),
                    Opis = "uspješno implementirao rješenje za administraciju članova"
                },
                new DodatniProizvoljniUnosi
                {
                    ClanId = clanovi.Single(i => i.Prezime == "Rogulj").ClanId,
                    DatumZapisa = DateTime.Parse("2019-02-01"),
                    Opis = "odlučija bit linčina i prepustit sve Hrvoju :) "
                }
            };
            foreach(DodatniProizvoljniUnosi s in dodatniProizvoljniUnosi)
            {
                context.DodatniProizvoljniUnosi.Add(s);
            }
            context.SaveChanges();

            var obraznovanja = new Obrazovanje[]
            {
                new Obrazovanje{
                    ClanId = clanovi.Single(i=> i.Prezime == "RogulJ").ClanId,
                    ObrazovnaUstanova = "PMF" ,
                    Od = DateTime.Parse("2005-10-01"),
                    Do = DateTime.Parse("2013-10-01")
                    }
            };

            foreach(Obrazovanje s in obraznovanja)
            {
                context.Obrazovanje.Add(s);
            }
            context.SaveChanges();

            var zapisiPoClanu = new ZapisiPoClanu[]
            {
                new ZapisiPoClanu{
                    ClanId = clanovi.Single(i => i.Prezime == "Rogulj").ClanId,
                    TipoviZapisaId = tipoviZapisa.Single(i => i.Naziv == "Suspenzija").TipoviZapisaId,
                    DatumZapisa = DateTime.Parse("2019-02-01"),
                    Opis = "2 miseca suspenzije zbog linosti"
                }
            };
            foreach(ZapisiPoClanu s in zapisiPoClanu)
            {
                context.ZapisiPoClanu.Add(s);
            }
            context.SaveChanges();

            var lijecnicki = new LijecnickiPregledi[]
            {
                new LijecnickiPregledi
                {
                    ClanId = clanovi.Single(i => i.Prezime == "Rogulj").ClanId,
                    VrijediOd = DateTime.Parse("2018-11-01"),
                    VrijediDo = DateTime.Parse("2019-05-01")
                },
                new LijecnickiPregledi
                {
                    ClanId = clanovi.Single(i => i.Prezime == "Sabljić").ClanId,
                    VrijediOd = DateTime.Parse("2019-01-15"),
                    VrijediDo = DateTime.Parse("2019-07-15")
                }
            };
            foreach(LijecnickiPregledi s in lijecnicki)
            {
                context.LijecnickiPregledi.Add(s);
            }
            context.SaveChanges();

            var uplate = new UplataClanarine[]
            {
                new UplataClanarine
                {
                    ClanId = clanovi.Single(i => i.Prezime == "Rogulj").ClanId,
                    DatumUplate = DateTime.Today,
                    MjesecGodina = DateTime.Parse("2019-01-01")
                },
                new UplataClanarine
                {
                    ClanId = clanovi.Single(i => i.Prezime == "Rogulj").ClanId,
                    DatumUplate = DateTime.Today,
                    MjesecGodina = DateTime.Parse("2019-02-01")
                },
                new UplataClanarine
                {
                    ClanId = clanovi.Single(i => i.Prezime == "Sabljić").ClanId,
                    DatumUplate = DateTime.Today,
                    MjesecGodina = DateTime.Parse("2019-01-01")
                }

            };

            foreach(UplataClanarine s in uplate)
            {
                context.UplataClanarine.Add(s);
            }
            context.SaveChanges();
        }
    }
}
