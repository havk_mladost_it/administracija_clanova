using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Database.Model
{
    [Table("Obrazovanje")]
    public class Obrazovanje
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ObrazovanjeId { get; set; }

        [Column(TypeName = "date")]
        [Required]
        public DateTime Od { get; set; }

        [Column(TypeName = "date")]
        public DateTime Do { get; set; }

        [Column(TypeName = "nvarchar(30)")]
        [Required]
        public String ObrazovnaUstanova { get; set; }

        [Required]
        public int ClanId { get; set; }
        public Clan Clan { get; set; }

        }
}