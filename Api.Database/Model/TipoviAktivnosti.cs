using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Database.Model
{
    [Table("TipoviAktivnosti")]
    public class TipoviAktivnosti
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int TipoviAktivnostiId { get; set; }

        [Column(TypeName = "nvarchar(30)")]
        [Required]
        public string NazivAktivnosti { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        [Required]
        public string Opis { get; set; }

        [Column(TypeName = "bit")]
        public bool Aktivan { get; set; }

        public ICollection<AktivnostiSudjelovanje> AktivnostiSudjelovanje { get; set; }
    }
}