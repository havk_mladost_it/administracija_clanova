using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Database.Model
{
    [Table("Karakteristike")]
    public class Karakteristike
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int KarakteristikeId { get; set; }

        [Column(TypeName = "nvarchar(30)")]
        [Required]
        public string Naziv { get; set; }

        [Column(TypeName = "nvarchar(200)")]
        public string Opis { get; set; }

        [Column(TypeName = "nvarchar(10)")]
        public string MjernaJedinica { get; set; }

        public ICollection<KarakteristikeSportasa> KarakteristikeSportasa { get; set; }
    }
}