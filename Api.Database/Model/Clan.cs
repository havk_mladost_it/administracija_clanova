
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Api.Database.Model
{

    [Table("Clan")]
    public class Clan
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]

        string _ime;
        string _prezime;
        public int ClanId { get; set; }

        [Column(TypeName = "nvarchar(30)")]
        [Required]
        public string Ime
        {
            get { return _ime; }
            set
            {
                _ime = FirstCharToUpper(value);
            }
        }

        [Column(TypeName = "nvarchar(30)")]
        [Required]
        public string Prezime
        {
            get { return _prezime; }
            set
            {
                _prezime = FirstCharToUpper(value);
            }
        }

        [Column(TypeName = "nchar(1)")]
        [Required]
        public string Spol { get; set; }

        [Column(TypeName = "char(11)")]
        [Required]
        public string OIB { get; set; }

        [Column(TypeName = "date")]
        [Required]
        public DateTime Rodendan { get; set; }

        [Column(TypeName = "varchar(20)")]
        public string Telefon { get; set; }

        [Column(TypeName = "varchar(30)")]
        public string Email { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        [Required]
        public string Adresa { get; set; }

        [Column(TypeName = "nvarchar(30)")]
        public string Zvanje { get; set; }

        [Column(TypeName = "nvarchar(30)")]
        public string Zanimanje { get; set; }

        [Column(TypeName = "nvarchar(30)")]
        public string AkademskaTitula { get; set; }

        [Column(TypeName = "nvarchar(30)")]
        public string RegistracijaHVS { get; set; }

        public int? SkrbnikId { get; set; }
        public Skrbnik PodaciSkrbnika { get; set; }

        public ICollection<Duznosti> Duznosti { get; set; }
        public ICollection<Clanarine> Clanarine { get; set; }
        public ICollection<Obrazovanje> Obrazovanje { get; set; }
        public ICollection<ZapisiPoClanu> ZapisiPoClanu { get; set; }
        public ICollection<AktivnostiSudjelovanje> AktivnostiSudjelovanje { get; set; }
        public ICollection<DodatniProizvoljniUnosi> DodatniProizvoljniUnosi { get; set; }

        public ICollection<KarakteristikeSportasa> KarakteristikeSportasa { get; set; }

        public ICollection<UplataClanarine> UplataClanarine { get; set; }

        public ICollection<LijecnickiPregledi> LijecnickiPregledi { get; set; }

        public Treneri Treneri { get; set; }

        public ICollection<PripadnostSekciji> PripadnostSekciji { get; set; }

        private static string FirstCharToUpper(string input)
        {
            if (String.IsNullOrEmpty(input))
                throw new ArgumentException("Argument must be valid string value!");
            return input.First().ToString().ToUpper() + String.Join("", input.Skip(1)).ToLower();
        }
    }
}