
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Database.Model
{
    [Table("TreneriDodjele")]
    public class TreneriDodjele
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int TreneriDodjeleId { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        [Required]
        public string Kvalifikacije { get; set; }

        [Column(TypeName = "decimal")]
        [Required]
        public decimal GodineStaza { get; set; }

        [Column(TypeName = "bit")]
        public bool? Reprezentacija { get; set; }

        [Column(TypeName = "nvarchar(30)")]
        [Required]
        public string KategorijaTrenera { get; set; }

        [Column(TypeName = "nvarchar(50)")]
        public string ReprezentacijaPosada { get; set; }

        [Column(TypeName = "date")]
        [Required]
        public DateTime Od { get; set; }

        [Column(TypeName = "date")]
        public DateTime Do { get; set; }

        public int SekcijaId { get; set; }
        public Sekcija Sekcija { get; set; }

        public int TreneriId { get; set; }
        public Treneri Treneri { get; set; }
    }
}