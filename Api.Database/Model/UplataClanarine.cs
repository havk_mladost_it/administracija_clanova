using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Api.Database.Model
{
    [Table("UplataClanarine")]
    public class UplataClanarine
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int UplataClanarineId { get; set; }

        [Column(TypeName = "date")]
        [Required]
        public DateTime DatumUplate { get; set; }

        [Column(TypeName = "date")]
        [Required]
        public DateTime MjesecGodina { get; set; }

        public int ClanId { get; set; }
        public Clan Clan { get; set; }

    }
}