using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace Api.Database.Model
{
    [Table("PripadnostSekciji")]
    public class PripadnostSekciji
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int PripadnostSekcijiId { get; set; }

        [Column(TypeName = "bit")]
        [Required]
        public bool RedovniClan { get; set; }

        [Column(TypeName = "date")]
        [Required]
        public DateTime Od { get; set; }

        [Column(TypeName = "date")]
        public DateTime Do { get; set; }
        public int ClanId { get; set; }
        public Clan Clan { get; set; }

        public int? TipoviKategorizacijeId { get; set; }
        public TipoviKategorizacije TipoviKategorizacije { get; set; }

        public int SekcijaId { get; set; }
        public Sekcija Sekcija { get; set; }

        public int TreneriId { get; set; }
        public Treneri Treneri { get; set; }

    }
}