﻿using Microsoft.EntityFrameworkCore;
using Api.Database.Model;
namespace Api.Database
{
    public class ApiContext : DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options) : base(options) { }
        //osobni podaci
        public DbSet<Clan> Clan { get; set; }
        public DbSet<Skrbnik> PodaciSkrbnika { get; set; }
        //parametarske tablice
        public DbSet<TipoviDuznosti> TipoviDuznosti { get; set; }
        public DbSet<TipoviZapisa> TipoviZapisa { get; set; }
        public DbSet<TipoviClanarina> TipoviClanarina { get; set; }
        public DbSet<TipoviAktivnosti> TipoviAktivnosti { get; set; }
        public DbSet<Sekcija> Sekcija { get; set; }
        public DbSet<TipoviKategorizacije> TipoviKategorizacije { get; set; }
        public DbSet<Karakteristike> Karakteristike { get; set; }
        //transakcijske tablice
        public DbSet<Duznosti> Duznosti { get; set; }
        public DbSet<Clanarine> Clanarine { get; set; }
        public DbSet<Obrazovanje> Obrazovanje { get; set; }
        public DbSet<ZapisiPoClanu> ZapisiPoClanu { get; set; }
        public DbSet<AktivnostiSudjelovanje> AktivnostiSudjelovanje { get; set; }
        public DbSet<DodatniProizvoljniUnosi> DodatniProizvoljniUnosi { get; set; }

        public DbSet<PripadnostSekciji> PripadnostSekciji { get; set; }
        public DbSet<Treneri> Treneri { get; set; }
        public DbSet<TreneriDodjele> TreneriDodjele { get; set; }
        public DbSet<KarakteristikeSportasa> KarakteristikeSportasa { get; set; }

        public DbSet<UplataClanarine> UplataClanarine { get; set; }

        public DbSet<LijecnickiPregledi> LijecnickiPregledi { get; set; }
    }
}
