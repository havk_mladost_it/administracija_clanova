﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Api.Database.Migrations
{
    public partial class _initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Karakteristike",
                columns: table => new
                {
                    KarakteristikeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Naziv = table.Column<string>(type: "nvarchar(30)", nullable: false),
                    Opis = table.Column<string>(type: "nvarchar(200)", nullable: true),
                    MjernaJedinica = table.Column<string>(type: "nvarchar(10)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Karakteristike", x => x.KarakteristikeId);
                });

            migrationBuilder.CreateTable(
                name: "PodaciSkrbnika",
                columns: table => new
                {
                    SkrbnikId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Ime = table.Column<string>(type: "nvarchar(30)", nullable: false),
                    Prezime = table.Column<string>(type: "nvarchar(30)", nullable: false),
                    Spol = table.Column<string>(type: "nchar(1)", nullable: false),
                    OIB = table.Column<string>(type: "char(11)", nullable: false),
                    Telefon = table.Column<string>(type: "varchar(20)", nullable: true),
                    Email = table.Column<string>(type: "varchar(30)", nullable: true),
                    Adresa = table.Column<string>(type: "nvarchar(50)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PodaciSkrbnika", x => x.SkrbnikId);
                });

            migrationBuilder.CreateTable(
                name: "Sekcija",
                columns: table => new
                {
                    SekcijaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Sifra = table.Column<string>(type: "nvarchar(5)", nullable: false),
                    Naziv = table.Column<string>(type: "nvarchar(30)", nullable: false),
                    Opis = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    Aktivno = table.Column<bool>(type: "bit", nullable: false),
                    Natjecateljska = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sekcija", x => x.SekcijaId);
                });

            migrationBuilder.CreateTable(
                name: "TipoviAktivnosti",
                columns: table => new
                {
                    TipoviAktivnostiId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NazivAktivnosti = table.Column<string>(type: "nvarchar(20)", nullable: false),
                    Opis = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    Aktivan = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoviAktivnosti", x => x.TipoviAktivnostiId);
                });

            migrationBuilder.CreateTable(
                name: "TipoviClanarina",
                columns: table => new
                {
                    TipoviClanarinaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NazivClanarine = table.Column<string>(type: "nvarchar(30)", nullable: false),
                    Opis = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    IznosClanarine = table.Column<decimal>(type: "decimal(5,2)", nullable: false),
                    Grupna = table.Column<bool>(type: "bit", nullable: false),
                    Aktivno = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoviClanarina", x => x.TipoviClanarinaId);
                });

            migrationBuilder.CreateTable(
                name: "TipoviDuznosti",
                columns: table => new
                {
                    TipoviDuznostiId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NazivDuznosti = table.Column<string>(type: "nvarchar(20)", nullable: false),
                    Opis = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    Aktivan = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoviDuznosti", x => x.TipoviDuznostiId);
                });

            migrationBuilder.CreateTable(
                name: "TipoviKategorizacije",
                columns: table => new
                {
                    TipoviKategorizacijeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Naziv = table.Column<string>(type: "nvarchar(20)", nullable: false),
                    Aktivno = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoviKategorizacije", x => x.TipoviKategorizacijeId);
                });

            migrationBuilder.CreateTable(
                name: "TipoviZapisa",
                columns: table => new
                {
                    TipoviZapisaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    KodZapisa = table.Column<string>(type: "nvarchar(5)", nullable: false),
                    Naziv = table.Column<string>(type: "nvarchar(20)", nullable: false),
                    Opis = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    Aktivan = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoviZapisa", x => x.TipoviZapisaId);
                });

            migrationBuilder.CreateTable(
                name: "Clan",
                columns: table => new
                {
                    ClanId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Ime = table.Column<string>(type: "nvarchar(30)", nullable: false),
                    Prezime = table.Column<string>(type: "nvarchar(30)", nullable: false),
                    Spol = table.Column<string>(type: "nchar(1)", nullable: false),
                    OIB = table.Column<string>(type: "char(11)", nullable: false),
                    Rodendan = table.Column<DateTime>(type: "date", nullable: false),
                    Telefon = table.Column<string>(type: "varchar(20)", nullable: true),
                    Email = table.Column<string>(type: "varchar(30)", nullable: true),
                    Adresa = table.Column<string>(type: "nvarchar(50)", nullable: false),
                    Zvanje = table.Column<string>(type: "nvarchar(30)", nullable: true),
                    Zanimanje = table.Column<string>(type: "nvarchar(30)", nullable: true),
                    AkademskaTitula = table.Column<string>(type: "nvarchar(30)", nullable: true),
                    RegistracijaHVS = table.Column<string>(type: "nvarchar(30)", nullable: true),
                    SkrbnikId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clan", x => x.ClanId);
                    table.ForeignKey(
                        name: "FK_Clan_PodaciSkrbnika_SkrbnikId",
                        column: x => x.SkrbnikId,
                        principalTable: "PodaciSkrbnika",
                        principalColumn: "SkrbnikId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AktivnostiSudjelovanje",
                columns: table => new
                {
                    AktivnostiSudjelovanjeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DatumAktivnosti = table.Column<DateTime>(type: "date", nullable: false),
                    ClanId = table.Column<int>(nullable: false),
                    TipoviAKtivnostiId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AktivnostiSudjelovanje", x => x.AktivnostiSudjelovanjeId);
                    table.ForeignKey(
                        name: "FK_AktivnostiSudjelovanje_Clan_ClanId",
                        column: x => x.ClanId,
                        principalTable: "Clan",
                        principalColumn: "ClanId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AktivnostiSudjelovanje_TipoviAktivnosti_TipoviAKtivnostiId",
                        column: x => x.TipoviAKtivnostiId,
                        principalTable: "TipoviAktivnosti",
                        principalColumn: "TipoviAktivnostiId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Clanarine",
                columns: table => new
                {
                    ClanarineId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Od = table.Column<DateTime>(type: "date", nullable: false),
                    Do = table.Column<DateTime>(type: "date", nullable: false),
                    NositeljGrupne = table.Column<bool>(type: "bit", nullable: false),
                    NositeljGrupneId = table.Column<int>(nullable: false),
                    ClanId = table.Column<int>(nullable: false),
                    TipoviClanarinaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clanarine", x => x.ClanarineId);
                    table.ForeignKey(
                        name: "FK_Clanarine_Clan_NositeljGrupneId",
                        column: x => x.NositeljGrupneId,
                        principalTable: "Clan",
                        principalColumn: "ClanId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Clanarine_TipoviClanarina_TipoviClanarinaId",
                        column: x => x.TipoviClanarinaId,
                        principalTable: "TipoviClanarina",
                        principalColumn: "TipoviClanarinaId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DodatniProizvoljniUnosi",
                columns: table => new
                {
                    DodatniProizvoljniUnosiId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DatumZapisa = table.Column<DateTime>(type: "date", nullable: false),
                    Opis = table.Column<string>(nullable: true),
                    ClanId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DodatniProizvoljniUnosi", x => x.DodatniProizvoljniUnosiId);
                    table.ForeignKey(
                        name: "FK_DodatniProizvoljniUnosi_Clan_ClanId",
                        column: x => x.ClanId,
                        principalTable: "Clan",
                        principalColumn: "ClanId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Duznosti",
                columns: table => new
                {
                    DuznostiId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Od = table.Column<DateTime>(type: "date", nullable: false),
                    Do = table.Column<DateTime>(type: "date", nullable: false),
                    ClanId = table.Column<int>(nullable: false),
                    TipoviDuznostiId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Duznosti", x => x.DuznostiId);
                    table.ForeignKey(
                        name: "FK_Duznosti_Clan_ClanId",
                        column: x => x.ClanId,
                        principalTable: "Clan",
                        principalColumn: "ClanId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Duznosti_TipoviDuznosti_TipoviDuznostiId",
                        column: x => x.TipoviDuznostiId,
                        principalTable: "TipoviDuznosti",
                        principalColumn: "TipoviDuznostiId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "KarakteristikeSportasa",
                columns: table => new
                {
                    KarakteristikeSportasaId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Vrijednost = table.Column<decimal>(type: "decimal", nullable: false),
                    DatumOpisa = table.Column<DateTime>(type: "date", nullable: false),
                    ClanId = table.Column<int>(nullable: false),
                    KarkateristikeId = table.Column<int>(nullable: false),
                    KarakteristikeId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KarakteristikeSportasa", x => x.KarakteristikeSportasaId);
                    table.ForeignKey(
                        name: "FK_KarakteristikeSportasa_Clan_ClanId",
                        column: x => x.ClanId,
                        principalTable: "Clan",
                        principalColumn: "ClanId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_KarakteristikeSportasa_Karakteristike_KarakteristikeId",
                        column: x => x.KarakteristikeId,
                        principalTable: "Karakteristike",
                        principalColumn: "KarakteristikeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "LijecnickiPregledi",
                columns: table => new
                {
                    LijecnickiPreglediId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    VrijediOd = table.Column<DateTime>(type: "date", nullable: false),
                    VrijediDo = table.Column<DateTime>(type: "date", nullable: false),
                    ClanId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LijecnickiPregledi", x => x.LijecnickiPreglediId);
                    table.ForeignKey(
                        name: "FK_LijecnickiPregledi_Clan_ClanId",
                        column: x => x.ClanId,
                        principalTable: "Clan",
                        principalColumn: "ClanId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Obrazovanje",
                columns: table => new
                {
                    ObrazovanjeId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Od = table.Column<DateTime>(type: "date", nullable: false),
                    Do = table.Column<DateTime>(type: "date", nullable: false),
                    ObrazovnaUstanova = table.Column<string>(type: "nvarchar(30)", nullable: false),
                    ClanId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Obrazovanje", x => x.ObrazovanjeId);
                    table.ForeignKey(
                        name: "FK_Obrazovanje_Clan_ClanId",
                        column: x => x.ClanId,
                        principalTable: "Clan",
                        principalColumn: "ClanId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Treneri",
                columns: table => new
                {
                    TreneriId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Ime = table.Column<string>(type: "nvarchar(30)", nullable: false),
                    Prezime = table.Column<string>(type: "nvarchar(30)", nullable: false),
                    Aktivan = table.Column<bool>(type: "bit", nullable: false),
                    ClanId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Treneri", x => x.TreneriId);
                    table.ForeignKey(
                        name: "FK_Treneri_Clan_ClanId",
                        column: x => x.ClanId,
                        principalTable: "Clan",
                        principalColumn: "ClanId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UplataClanarine",
                columns: table => new
                {
                    UplataClanarineId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DatumUplate = table.Column<DateTime>(type: "date", nullable: false),
                    MjesecGodina = table.Column<DateTime>(type: "date", nullable: false),
                    ClanId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UplataClanarine", x => x.UplataClanarineId);
                    table.ForeignKey(
                        name: "FK_UplataClanarine_Clan_ClanId",
                        column: x => x.ClanId,
                        principalTable: "Clan",
                        principalColumn: "ClanId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ZapisiPoClanu",
                columns: table => new
                {
                    ZapisiPoClanuId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DatumZapisa = table.Column<DateTime>(type: "date", nullable: false),
                    Opis = table.Column<string>(type: "nvarchar(100)", nullable: true),
                    ClanId = table.Column<int>(nullable: false),
                    TipoviZapisaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ZapisiPoClanu", x => x.ZapisiPoClanuId);
                    table.ForeignKey(
                        name: "FK_ZapisiPoClanu_Clan_ClanId",
                        column: x => x.ClanId,
                        principalTable: "Clan",
                        principalColumn: "ClanId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ZapisiPoClanu_TipoviZapisa_TipoviZapisaId",
                        column: x => x.TipoviZapisaId,
                        principalTable: "TipoviZapisa",
                        principalColumn: "TipoviZapisaId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PripadnostSekciji",
                columns: table => new
                {
                    PripadnostSekcijiId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RedovniClan = table.Column<bool>(type: "bit", nullable: false),
                    Od = table.Column<DateTime>(type: "date", nullable: false),
                    Do = table.Column<DateTime>(type: "date", nullable: false),
                    ClanId = table.Column<int>(nullable: false),
                    TipoviKategorizacijeId = table.Column<int>(nullable: true),
                    SekcijaId = table.Column<int>(nullable: false),
                    TreneriId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PripadnostSekciji", x => x.PripadnostSekcijiId);
                    table.ForeignKey(
                        name: "FK_PripadnostSekciji_Clan_ClanId",
                        column: x => x.ClanId,
                        principalTable: "Clan",
                        principalColumn: "ClanId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PripadnostSekciji_Sekcija_SekcijaId",
                        column: x => x.SekcijaId,
                        principalTable: "Sekcija",
                        principalColumn: "SekcijaId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PripadnostSekciji_TipoviKategorizacije_TipoviKategorizacijeId",
                        column: x => x.TipoviKategorizacijeId,
                        principalTable: "TipoviKategorizacije",
                        principalColumn: "TipoviKategorizacijeId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_PripadnostSekciji_Treneri_TreneriId",
                        column: x => x.TreneriId,
                        principalTable: "Treneri",
                        principalColumn: "TreneriId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TreneriDodjele",
                columns: table => new
                {
                    TreneriDodjeleId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Kvalifikacije = table.Column<string>(type: "nvarchar(100)", nullable: false),
                    GodineStaža = table.Column<decimal>(type: "decimal", nullable: false),
                    Reprezentacija = table.Column<bool>(type: "bit", nullable: true),
                    KategorijaTrenera = table.Column<string>(type: "nvarchar(30)", nullable: false),
                    ReprezentacijaPosada = table.Column<string>(type: "nvarchar(50)", nullable: true),
                    Od = table.Column<DateTime>(type: "date", nullable: false),
                    Do = table.Column<DateTime>(type: "date", nullable: false),
                    SekcijaId = table.Column<int>(nullable: false),
                    TreneriId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TreneriDodjele", x => x.TreneriDodjeleId);
                    table.ForeignKey(
                        name: "FK_TreneriDodjele_Sekcija_SekcijaId",
                        column: x => x.SekcijaId,
                        principalTable: "Sekcija",
                        principalColumn: "SekcijaId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TreneriDodjele_Treneri_TreneriId",
                        column: x => x.TreneriId,
                        principalTable: "Treneri",
                        principalColumn: "TreneriId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AktivnostiSudjelovanje_ClanId",
                table: "AktivnostiSudjelovanje",
                column: "ClanId");

            migrationBuilder.CreateIndex(
                name: "IX_AktivnostiSudjelovanje_TipoviAKtivnostiId",
                table: "AktivnostiSudjelovanje",
                column: "TipoviAKtivnostiId");

            migrationBuilder.CreateIndex(
                name: "IX_Clan_SkrbnikId",
                table: "Clan",
                column: "SkrbnikId",
                unique: true,
                filter: "[SkrbnikId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Clanarine_NositeljGrupneId",
                table: "Clanarine",
                column: "NositeljGrupneId");

            migrationBuilder.CreateIndex(
                name: "IX_Clanarine_TipoviClanarinaId",
                table: "Clanarine",
                column: "TipoviClanarinaId");

            migrationBuilder.CreateIndex(
                name: "IX_DodatniProizvoljniUnosi_ClanId",
                table: "DodatniProizvoljniUnosi",
                column: "ClanId");

            migrationBuilder.CreateIndex(
                name: "IX_Duznosti_ClanId",
                table: "Duznosti",
                column: "ClanId");

            migrationBuilder.CreateIndex(
                name: "IX_Duznosti_TipoviDuznostiId",
                table: "Duznosti",
                column: "TipoviDuznostiId");

            migrationBuilder.CreateIndex(
                name: "IX_KarakteristikeSportasa_ClanId",
                table: "KarakteristikeSportasa",
                column: "ClanId");

            migrationBuilder.CreateIndex(
                name: "IX_KarakteristikeSportasa_KarakteristikeId",
                table: "KarakteristikeSportasa",
                column: "KarakteristikeId");

            migrationBuilder.CreateIndex(
                name: "IX_LijecnickiPregledi_ClanId",
                table: "LijecnickiPregledi",
                column: "ClanId");

            migrationBuilder.CreateIndex(
                name: "IX_Obrazovanje_ClanId",
                table: "Obrazovanje",
                column: "ClanId");

            migrationBuilder.CreateIndex(
                name: "IX_PripadnostSekciji_ClanId",
                table: "PripadnostSekciji",
                column: "ClanId");

            migrationBuilder.CreateIndex(
                name: "IX_PripadnostSekciji_SekcijaId",
                table: "PripadnostSekciji",
                column: "SekcijaId");

            migrationBuilder.CreateIndex(
                name: "IX_PripadnostSekciji_TipoviKategorizacijeId",
                table: "PripadnostSekciji",
                column: "TipoviKategorizacijeId");

            migrationBuilder.CreateIndex(
                name: "IX_PripadnostSekciji_TreneriId",
                table: "PripadnostSekciji",
                column: "TreneriId");

            migrationBuilder.CreateIndex(
                name: "IX_Treneri_ClanId",
                table: "Treneri",
                column: "ClanId",
                unique: true,
                filter: "[ClanId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_TreneriDodjele_SekcijaId",
                table: "TreneriDodjele",
                column: "SekcijaId");

            migrationBuilder.CreateIndex(
                name: "IX_TreneriDodjele_TreneriId",
                table: "TreneriDodjele",
                column: "TreneriId");

            migrationBuilder.CreateIndex(
                name: "IX_UplataClanarine_ClanId",
                table: "UplataClanarine",
                column: "ClanId");

            migrationBuilder.CreateIndex(
                name: "IX_ZapisiPoClanu_ClanId",
                table: "ZapisiPoClanu",
                column: "ClanId");

            migrationBuilder.CreateIndex(
                name: "IX_ZapisiPoClanu_TipoviZapisaId",
                table: "ZapisiPoClanu",
                column: "TipoviZapisaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AktivnostiSudjelovanje");

            migrationBuilder.DropTable(
                name: "Clanarine");

            migrationBuilder.DropTable(
                name: "DodatniProizvoljniUnosi");

            migrationBuilder.DropTable(
                name: "Duznosti");

            migrationBuilder.DropTable(
                name: "KarakteristikeSportasa");

            migrationBuilder.DropTable(
                name: "LijecnickiPregledi");

            migrationBuilder.DropTable(
                name: "Obrazovanje");

            migrationBuilder.DropTable(
                name: "PripadnostSekciji");

            migrationBuilder.DropTable(
                name: "TreneriDodjele");

            migrationBuilder.DropTable(
                name: "UplataClanarine");

            migrationBuilder.DropTable(
                name: "ZapisiPoClanu");

            migrationBuilder.DropTable(
                name: "TipoviAktivnosti");

            migrationBuilder.DropTable(
                name: "TipoviClanarina");

            migrationBuilder.DropTable(
                name: "TipoviDuznosti");

            migrationBuilder.DropTable(
                name: "Karakteristike");

            migrationBuilder.DropTable(
                name: "TipoviKategorizacije");

            migrationBuilder.DropTable(
                name: "Sekcija");

            migrationBuilder.DropTable(
                name: "Treneri");

            migrationBuilder.DropTable(
                name: "TipoviZapisa");

            migrationBuilder.DropTable(
                name: "Clan");

            migrationBuilder.DropTable(
                name: "PodaciSkrbnika");
        }
    }
}
