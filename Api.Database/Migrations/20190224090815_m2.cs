﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Api.Database.Migrations
{
    public partial class m2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "GodineStaža",
                table: "TreneriDodjele",
                newName: "GodineStaza");

            migrationBuilder.AlterColumn<string>(
                name: "Naziv",
                table: "TipoviZapisa",
                type: "nvarchar(30)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(20)");

            migrationBuilder.AlterColumn<string>(
                name: "NazivDuznosti",
                table: "TipoviDuznosti",
                type: "nvarchar(40)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(20)");

            migrationBuilder.AlterColumn<decimal>(
                name: "IznosClanarine",
                table: "TipoviClanarina",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(5,2)");

            migrationBuilder.AlterColumn<string>(
                name: "NazivAktivnosti",
                table: "TipoviAktivnosti",
                type: "nvarchar(30)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(20)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "GodineStaza",
                table: "TreneriDodjele",
                newName: "GodineStaža");

            migrationBuilder.AlterColumn<string>(
                name: "Naziv",
                table: "TipoviZapisa",
                type: "nvarchar(20)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(30)");

            migrationBuilder.AlterColumn<string>(
                name: "NazivDuznosti",
                table: "TipoviDuznosti",
                type: "nvarchar(20)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(40)");

            migrationBuilder.AlterColumn<decimal>(
                name: "IznosClanarine",
                table: "TipoviClanarina",
                type: "decimal(5,2)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<string>(
                name: "NazivAktivnosti",
                table: "TipoviAktivnosti",
                type: "nvarchar(20)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(30)");
        }
    }
}
