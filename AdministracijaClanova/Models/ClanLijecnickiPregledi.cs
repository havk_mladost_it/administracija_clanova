using System;
using System.Collections.Generic;
using Api.Database.Model;

namespace AdministracijaClanova
{

    public class ClanLijecnickiPregledi
    {

        public Clan Clan { get; set; }
        public List<LijecnickiPregledi> LijecnickiPregledi { get; set; }
    }

}