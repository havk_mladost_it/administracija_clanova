﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Api.Database;
using Api.Database.Model;

namespace AdministracijaClanova.Controllers
{
    public class UplataClanarineController : Controller
    {
        private readonly ApiContext _context;

        public UplataClanarineController(ApiContext context)
        {
            _context = context;
        }

        // GET: UplataClanarine
        public async Task<IActionResult> Index()
        {
            var apiContext = _context.UplataClanarine.Include(u => u.Clan);
            return View(await apiContext.ToListAsync());
        }

        // GET: UplataClanarine/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var uplataClanarine = await _context.UplataClanarine
                .Include(u => u.Clan)
                .SingleOrDefaultAsync(m => m.UplataClanarineId == id);
            if (uplataClanarine == null)
            {
                return NotFound();
            }

            return View(uplataClanarine);
        }

        // GET: UplataClanarine/Create
        public IActionResult Create()
        {
            ViewData["ClanId"] = new SelectList(_context.Clan, "ClanId", "Prezime");
            return View();
        }

        // POST: UplataClanarine/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("UplataClanarineId,DatumUplate,MjesecGodina,ClanId")] UplataClanarine uplataClanarine)
        {
            if (ModelState.IsValid)
            {
                _context.Add(uplataClanarine);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClanId"] = new SelectList(_context.Clan, "ClanId", "Adresa", uplataClanarine.ClanId);
            return View(uplataClanarine);
        }

        // GET: UplataClanarine/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var uplataClanarine = await _context.UplataClanarine.SingleOrDefaultAsync(m => m.UplataClanarineId == id);
            if (uplataClanarine == null)
            {
                return NotFound();
            }
            ViewData["ClanId"] = new SelectList(_context.Clan, "ClanId", "Adresa", uplataClanarine.ClanId);
            return View(uplataClanarine);
        }

        // POST: UplataClanarine/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("UplataClanarineId,DatumUplate,MjesecGodina,ClanId")] UplataClanarine uplataClanarine)
        {
            if (id != uplataClanarine.UplataClanarineId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(uplataClanarine);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UplataClanarineExists(uplataClanarine.UplataClanarineId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClanId"] = new SelectList(_context.Clan, "ClanId", "Adresa", uplataClanarine.ClanId);
            return View(uplataClanarine);
        }

        // GET: UplataClanarine/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var uplataClanarine = await _context.UplataClanarine
                .Include(u => u.Clan)
                .SingleOrDefaultAsync(m => m.UplataClanarineId == id);
            if (uplataClanarine == null)
            {
                return NotFound();
            }

            return View(uplataClanarine);
        }

        // POST: UplataClanarine/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var uplataClanarine = await _context.UplataClanarine.SingleOrDefaultAsync(m => m.UplataClanarineId == id);
            _context.UplataClanarine.Remove(uplataClanarine);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UplataClanarineExists(int id)
        {
            return _context.UplataClanarine.Any(e => e.UplataClanarineId == id);
        }
    }
}
