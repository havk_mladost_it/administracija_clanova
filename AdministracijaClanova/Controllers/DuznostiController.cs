using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Api.Database;
using Api.Database.Model;

namespace AdministracijaClanova.Controllers
{
    public class DuznostiController : Controller
    {
        private readonly ApiContext _context;

        public DuznostiController(ApiContext context)
        {
            _context = context;
        }

        // GET: Duznosti
        public async Task<IActionResult> Index()
        {
            var apiContext = _context.Duznosti.Include(d => d.Clan).Include(d => d.TipoviDuznosti);
            return View(await apiContext.ToListAsync());
        }

        // GET: Duznosti/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var duznosti = await _context.Duznosti
                .Include(d => d.Clan)
                .Include(d => d.TipoviDuznosti)
                .SingleOrDefaultAsync(m => m.DuznostiId == id);
            if (duznosti == null)
            {
                return NotFound();
            }

            return View(duznosti);
        }

        // GET: Duznosti/Create
        public IActionResult Create()
        {
            ViewData["ClanId"] = new SelectList(_context.Clan, "ClanId", "Adresa");
            ViewData["TipoviDuznostiId"] = new SelectList(_context.TipoviDuznosti, "TipoviDuznostiId", "NazivDuznosti");
            return View();
        }

        // POST: Duznosti/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DuznostiId,Od,Do,ClanId,TipoviDuznostiId")] Duznosti duznosti)
        {
            if (ModelState.IsValid)
            {
                _context.Add(duznosti);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClanId"] = new SelectList(_context.Clan, "ClanId", "Adresa", duznosti.ClanId);
            ViewData["TipoviDuznostiId"] = new SelectList(_context.TipoviDuznosti, "TipoviDuznostiId", "NazivDuznosti", duznosti.TipoviDuznostiId);
            return View(duznosti);
        }

        // GET: Duznosti/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var duznosti = await _context.Duznosti.SingleOrDefaultAsync(m => m.DuznostiId == id);
            if (duznosti == null)
            {
                return NotFound();
            }
            ViewData["ClanId"] = new SelectList(_context.Clan, "ClanId", "Adresa", duznosti.ClanId);
            ViewData["TipoviDuznostiId"] = new SelectList(_context.TipoviDuznosti, "TipoviDuznostiId", "NazivDuznosti", duznosti.TipoviDuznostiId);
            return View(duznosti);
        }

        // POST: Duznosti/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("DuznostiId,Od,Do,ClanId,TipoviDuznostiId")] Duznosti duznosti)
        {
            if (id != duznosti.DuznostiId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(duznosti);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DuznostiExists(duznosti.DuznostiId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClanId"] = new SelectList(_context.Clan, "ClanId", "Adresa", duznosti.ClanId);
            ViewData["TipoviDuznostiId"] = new SelectList(_context.TipoviDuznosti, "TipoviDuznostiId", "NazivDuznosti", duznosti.TipoviDuznostiId);
            return View(duznosti);
        }

        // GET: Duznosti/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var duznosti = await _context.Duznosti
                .Include(d => d.Clan)
                .Include(d => d.TipoviDuznosti)
                .SingleOrDefaultAsync(m => m.DuznostiId == id);
            if (duznosti == null)
            {
                return NotFound();
            }

            return View(duznosti);
        }

        // POST: Duznosti/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var duznosti = await _context.Duznosti.SingleOrDefaultAsync(m => m.DuznostiId == id);
            _context.Duznosti.Remove(duznosti);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DuznostiExists(int id)
        {
            return _context.Duznosti.Any(e => e.DuznostiId == id);
        }
    }
}
