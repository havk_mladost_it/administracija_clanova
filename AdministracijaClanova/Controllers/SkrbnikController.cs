using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Api.Database;
using Api.Database.Model;

namespace AdministracijaClanova.Controllers
{
    public class SkrbnikController : Controller
    {
        private readonly ApiContext _context;

        public SkrbnikController(ApiContext context)
        {
            _context = context;
        }

        // GET: Skrbnik
        public async Task<IActionResult> Index()
        {
            return View(await _context.PodaciSkrbnika.ToListAsync());
        }

        // GET: Skrbnik/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var skrbnik = await _context.PodaciSkrbnika
                .SingleOrDefaultAsync(m => m.SkrbnikId == id);
            if (skrbnik == null)
            {
                return NotFound();
            }

            return View(skrbnik);
        }

        // GET: Skrbnik/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Skrbnik/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SkrbnikId,Ime,Prezime,Spol,OIB,Telefon,Email,Adresa")] Skrbnik skrbnik)
        {
            if (ModelState.IsValid)
            {
                _context.Add(skrbnik);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(skrbnik);
        }

        // GET: Skrbnik/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var skrbnik = await _context.PodaciSkrbnika.SingleOrDefaultAsync(m => m.SkrbnikId == id);
            if (skrbnik == null)
            {
                return NotFound();
            }
            return View(skrbnik);
        }

        // POST: Skrbnik/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("SkrbnikId,Ime,Prezime,Spol,OIB,Telefon,Email,Adresa")] Skrbnik skrbnik)
        {
            if (id != skrbnik.SkrbnikId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(skrbnik);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SkrbnikExists(skrbnik.SkrbnikId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(skrbnik);
        }

        // GET: Skrbnik/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var skrbnik = await _context.PodaciSkrbnika
                .SingleOrDefaultAsync(m => m.SkrbnikId == id);
            if (skrbnik == null)
            {
                return NotFound();
            }

            return View(skrbnik);
        }

        // POST: Skrbnik/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var skrbnik = await _context.PodaciSkrbnika.SingleOrDefaultAsync(m => m.SkrbnikId == id);
            _context.PodaciSkrbnika.Remove(skrbnik);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SkrbnikExists(int id)
        {
            return _context.PodaciSkrbnika.Any(e => e.SkrbnikId == id);
        }
    }
}
