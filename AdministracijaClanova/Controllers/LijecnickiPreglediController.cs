using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AdministracijaClanova;
using Api.Database;
using Api.Database.Model;

namespace AdministracijaClanova.Controllers
{
    public class LijecnickiPreglediController : Controller
    {
        private readonly ApiContext _context;

        public LijecnickiPreglediController(ApiContext context)
        {
            _context = context;
        }

        // GET: LijecnickiPregledi
        public async Task<IActionResult> Index(int? id)
        {
            var lpList = await _context.LijecnickiPregledi.Where(l => l.ClanId == id).ToListAsync();
            var clanObject = await _context.Clan.Where(clan => clan.ClanId == id).SingleOrDefaultAsync();
            ClanLijecnickiPregledi clp = new ClanLijecnickiPregledi { Clan = clanObject, LijecnickiPregledi = lpList };
            return View(clp);
        }

        // GET: LijecnickiPregledi/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lijecnickiPregledi = await _context.LijecnickiPregledi
                .Include(l => l.Clan)
                .SingleOrDefaultAsync(m => m.LijecnickiPreglediId == id);
            if (lijecnickiPregledi == null)
            {
                return NotFound();
            }

            return View(lijecnickiPregledi);
        }

        // GET: LijecnickiPregledi/Create
        public IActionResult Create(int? id)
        {
            ViewData["ClanId"] = new SelectList(_context.Clan, "ClanId", "Ime", id);
            return View(new LijecnickiPregledi { ClanId = (int)id });
        }

        // POST: LijecnickiPregledi/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("LijecnickiPreglediId,VrijediOd,VrijediDo,ClanId")] LijecnickiPregledi lijecnickiPregledi)
        {
            if (ModelState.IsValid)
            {
                _context.Add(lijecnickiPregledi);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index), new { id = lijecnickiPregledi.ClanId });
            }
            ViewData["ClanId"] = new SelectList(_context.Clan, "ClanId", "Ime", lijecnickiPregledi.ClanId);
            return View(lijecnickiPregledi);
        }

        // GET: LijecnickiPregledi/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lijecnickiPregledi = await _context.LijecnickiPregledi.SingleOrDefaultAsync(m => m.LijecnickiPreglediId == id);
            if (lijecnickiPregledi == null)
            {
                return NotFound();
            }
            ViewData["ClanId"] = new SelectList(_context.Clan, "ClanId", "Ime", lijecnickiPregledi.ClanId);
            return View(lijecnickiPregledi);
        }

        // POST: LijecnickiPregledi/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("LijecnickiPreglediId,VrijediOd,VrijediDo,ClanId")] LijecnickiPregledi lijecnickiPregledi)
        {
            if (id != lijecnickiPregledi.LijecnickiPreglediId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(lijecnickiPregledi);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LijecnickiPreglediExists(lijecnickiPregledi.LijecnickiPreglediId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index), new { id = lijecnickiPregledi.ClanId });
            }
            ViewData["ClanId"] = new SelectList(_context.Clan, "ClanId", "Adresa", lijecnickiPregledi.ClanId);
            return View(lijecnickiPregledi);
        }

        // GET: LijecnickiPregledi/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var lijecnickiPregledi = await _context.LijecnickiPregledi
                .Include(l => l.Clan)
                .SingleOrDefaultAsync(m => m.LijecnickiPreglediId == id);
            if (lijecnickiPregledi == null)
            {
                return NotFound();
            }

            return View(lijecnickiPregledi);
        }

        // POST: LijecnickiPregledi/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var lijecnickiPregledi = await _context.LijecnickiPregledi.SingleOrDefaultAsync(m => m.LijecnickiPreglediId == id);
            _context.LijecnickiPregledi.Remove(lijecnickiPregledi);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index), new { id = lijecnickiPregledi.ClanId });
        }

        private bool LijecnickiPreglediExists(int id)
        {
            return _context.LijecnickiPregledi.Any(e => e.LijecnickiPreglediId == id);
        }
    }
}
