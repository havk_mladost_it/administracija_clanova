using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Api.Database;
using Api.Database.Model;

namespace AdministracijaClanova.Controllers
{
    public class AktivnostiSudjelovanjeController : Controller
    {
        private readonly ApiContext _context;

        public AktivnostiSudjelovanjeController(ApiContext context)
        {
            _context = context;
        }

        // GET: AktivnostiSudjelovanje
        public async Task<IActionResult> Index()
        {
            var apiContext = _context.AktivnostiSudjelovanje.Include(a => a.Clan).Include(a => a.TipoviAktivnosti);
            return View(await apiContext.ToListAsync());
        }

        // GET: AktivnostiSudjelovanje/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aktivnostiSudjelovanje = await _context.AktivnostiSudjelovanje
                .Include(a => a.Clan)
                .Include(a => a.TipoviAktivnosti)
                .SingleOrDefaultAsync(m => m.AktivnostiSudjelovanjeId == id);
            if (aktivnostiSudjelovanje == null)
            {
                return NotFound();
            }

            return View(aktivnostiSudjelovanje);
        }

        // GET: AktivnostiSudjelovanje/Create
        public IActionResult Create()
        {
            ViewData["ClanId"] = new SelectList(_context.Clan, "ClanId", "Adresa");
            ViewData["TipoviAKtivnostiId"] = new SelectList(_context.TipoviAktivnosti, "TipoviAktivnostiId", "NazivAktivnosti");
            return View();
        }

        // POST: AktivnostiSudjelovanje/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AktivnostiSudjelovanjeId,DatumAktivnosti,ClanId,TipoviAKtivnostiId")] AktivnostiSudjelovanje aktivnostiSudjelovanje)
        {
            if (ModelState.IsValid)
            {
                _context.Add(aktivnostiSudjelovanje);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClanId"] = new SelectList(_context.Clan, "ClanId", "Adresa", aktivnostiSudjelovanje.ClanId);
            ViewData["TipoviAKtivnostiId"] = new SelectList(_context.TipoviAktivnosti, "TipoviAktivnostiId", "NazivAktivnosti", aktivnostiSudjelovanje.TipoviAKtivnostiId);
            return View(aktivnostiSudjelovanje);
        }

        // GET: AktivnostiSudjelovanje/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aktivnostiSudjelovanje = await _context.AktivnostiSudjelovanje.SingleOrDefaultAsync(m => m.AktivnostiSudjelovanjeId == id);
            if (aktivnostiSudjelovanje == null)
            {
                return NotFound();
            }
            ViewData["ClanId"] = new SelectList(_context.Clan, "ClanId", "Adresa", aktivnostiSudjelovanje.ClanId);
            ViewData["TipoviAKtivnostiId"] = new SelectList(_context.TipoviAktivnosti, "TipoviAktivnostiId", "NazivAktivnosti", aktivnostiSudjelovanje.TipoviAKtivnostiId);
            return View(aktivnostiSudjelovanje);
        }

        // POST: AktivnostiSudjelovanje/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("AktivnostiSudjelovanjeId,DatumAktivnosti,ClanId,TipoviAKtivnostiId")] AktivnostiSudjelovanje aktivnostiSudjelovanje)
        {
            if (id != aktivnostiSudjelovanje.AktivnostiSudjelovanjeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(aktivnostiSudjelovanje);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AktivnostiSudjelovanjeExists(aktivnostiSudjelovanje.AktivnostiSudjelovanjeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClanId"] = new SelectList(_context.Clan, "ClanId", "Adresa", aktivnostiSudjelovanje.ClanId);
            ViewData["TipoviAKtivnostiId"] = new SelectList(_context.TipoviAktivnosti, "TipoviAktivnostiId", "NazivAktivnosti", aktivnostiSudjelovanje.TipoviAKtivnostiId);
            return View(aktivnostiSudjelovanje);
        }

        // GET: AktivnostiSudjelovanje/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var aktivnostiSudjelovanje = await _context.AktivnostiSudjelovanje
                .Include(a => a.Clan)
                .Include(a => a.TipoviAktivnosti)
                .SingleOrDefaultAsync(m => m.AktivnostiSudjelovanjeId == id);
            if (aktivnostiSudjelovanje == null)
            {
                return NotFound();
            }

            return View(aktivnostiSudjelovanje);
        }

        // POST: AktivnostiSudjelovanje/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var aktivnostiSudjelovanje = await _context.AktivnostiSudjelovanje.SingleOrDefaultAsync(m => m.AktivnostiSudjelovanjeId == id);
            _context.AktivnostiSudjelovanje.Remove(aktivnostiSudjelovanje);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AktivnostiSudjelovanjeExists(int id)
        {
            return _context.AktivnostiSudjelovanje.Any(e => e.AktivnostiSudjelovanjeId == id);
        }
    }
}
