# Administracija clanova HAVK Mladost

Projekt koji pruža alate i sučelja za održavanje baze clanova kluba. 

## Dokumentacija i korisni linkovi

- Diagram baze podataka: `https://drive.google.com/file/d/107yKA0WVvNDYspm4g6vYXMoIHL5gKKVU/view?usp=sharing`
- Dokument baze podataka : `https://docs.google.com/document/d/1np21TB3kZrTxeL2qt-BTwsPvXo2qzR9nIevKvu8kbCs/edit?usp=sharing`

## Setup

### Local

## Migracija baze podataka
Pri svakom update-u modela baze potrebno je izvršiti novu migraciju i update-ati bazu podataka. 
Potreban je ovaj DummyDB console projekt za izvršavanje radnji migracije(?!) -> ovaj article pojašnjava use case: `https://garywoodfine.com/using-ef-core-in-a-separate-class-library-project/`

Ako je model promijenjen izvršiti migraciju i update baze sa navedne dvije komande iz Api.Database projekta(ovo je samo za dev način rada):
1. `dotnet ef --startup-project ../DummyDB migrations add <naziv_migracijske>`
2. `dotnet ef --startup-project ../DummyDB database update`

Potrebno napraviti seed i testne podatke koje ćemo moći koristiti za local i test environment!!! 

## Scaffolding
Za linux i genriranje koda iz terminala:
`dotnet aspnet-codegenerator controller -name ClanController -m Clan -dc ApContext --relativeFolderPath Controllers --useDefaultLayout --referenceScriptLibraries`

# Deployment to Azure 

## Local git deployment

Klonirati git repo App servisa kreiranog na Azure-u : `git clone https://havkmladostadministracija.scm.azurewebsites.net:443/havkmladostadministracija.git` sa sljedećim vjerodajnicama:
Username :`$havkmladostadministracija`
Password :`Yrb4YzCvkQWSXJ9wFBbWGF7TcatX50keoQNRooaCXwFszgn6iWZg9KzvhzZh`

Da bi aplikacija bila uspješno deploy-ana, potrebno je buildati projekt te izvršiti publish naredbu u git repo:
`dotnet build`(projekt AdministracijaClanova.csproj)
`dotnet publish -o <putanja_do_git_repo_foldera>`

Publish koji je izgeneriran commitati i pushnuti sa gitom na server.
